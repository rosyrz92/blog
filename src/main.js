import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'


import App from './App.vue'
import Admin from './Admin.vue'
import Home from './views/Home.vue'
import Login from './views/auth/Login.vue'
import Register from './views/auth/Register.vue'
import Create from './views/posts/Create.vue'
import Edit from './views/posts/Edit.vue'

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  }, 
   {
    path: '/register',
    name: 'Register',
    component: Register
  }, 
  {
        path: '/admin',
        component: Admin,
        children: [
           { 
           	path: 'home', 
           	name: 'Home', 
           	component: Home
           },
           {
			path: '/edit/:id',
			name: 'Edit',
			component: Edit
			},
			{
			path: '/create',
			name: 'Create',
			component: Create
			},
			{
			path: '/about',
			name: 'About',
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
			}

        ],
    },  
  ]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

const store = createStore({
	plugins:[
        createPersistedState()
    ],
  state () {
    return {
      token: null
    }
  },
  mutations: {
    saveToken (state, token) {
      state.token = token
    }
  },
  getters: {
    getToken (state) {
      return state.token;
    }
  }
})

const app = createApp(App);
app.use(router);
app.use(store);
app.mount('#app');