export default {
  '/': 'Home',
  '/edit/:id': 'Edit',
  '/create': 'Create'
}

